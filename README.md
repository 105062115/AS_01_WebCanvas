# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

**
Architecture:
  First,I add listeners on every object that need to be triggered by click or change in window.onload function.
  Next, I finish all the function that would be triggered by those events.

manual:
  brush,eraser: just click it and shake it.

  clear: clear all the things on the canvas.

  text: You can key the works in the box above, and choose the font style  ,size and color through those widgets on the left side.

  triangle: drag the mouse and release it(Just draw a line).
  You'll see a isosceles triangle lean against the mouse path.

  circle: drag the mouse and release it, you'll see a circle of the mouse path.

  rect: drag the mouse and release it, you'll see a rectangle.

  undo: click it and the canvas will return to the status before the last move.

  redo: click it and the canvas will return to the status before you clicked undo.

  download: download the picture you have drawn on the canvas.

  upload: upload a image and put it on the canvas.

function:
  1.brush
    <brushClick>
      when brushButton click it would be triggered.
      Change the mode to 0.
      get the value of strokeStyle and linewidth.
      Change the cursor.

  2.eraser
    <eraserClick>
      when eraserButton click it would be triggered.
      Change the mode to 1.
      set the value of strokeStyle and linewidth.
      Change the cursor.

  3.clear
    <clearClick>
      when clearButton click it would be triggered.
      reset the canvas to the original appearance.

  4.text
    <textInClick>
      when TextButton click it would be triggered.
      Change the mode to 2.
      Change the cursor.
    <textStyleChange>
      When font-style change, get the value of it.
    <textSizeChange>
      When font-size change, get the value of it.
    <keyInText>
      When the the text inside the input change, get the value of it.

  5.triangle
    <drawTriangle>
      Change the mode to 3.
      Change the cursor.

  6.circle
    <drawCircle>
      Change the mode to 4.
      Change the cursor.

  7.rectangle
    <drawCircle>
      Change the mode to 5.
      Change the cursor.

  8.undo
    <unDoClick>
      Let the step minus 1.
      Pull out the url that saved in the array.
      Clear the canvas and draw the url on it.

  9.redo
    <reDoClick>
      Let the step plus 1.
      Pull out the url that saved in the array.
      Clear the canvas and draw the url on it.

  10.upload
    Get the image and draw it on the canvas.

  11.download
    <downloadCanvas>
      Use todateURL to get the URL.
    <downloadS>
      randomly give it a name.

  12.hollow or filled(You can choose your 4,5,6,7 be hollow or filled)
    <filledChange>
      change the variable filled to filled of spaced(hollow).
      
  13.others
    <bruSizeChange>
      change the brush size to the new value after it changed;
    <cPush>
      save the canvas and transvert it into URL before push it into array.
    <mouseDown>
      Triggered when the mouse being clicked.
      Get the mouse position and do different things in different mode.
      (fill or stoke the text, set up some property and record the mouse position.)
    <mouseMove>
      Triggered when the mouse being draged.
      Do different things in different mode.
      (line to the mouse position when the mode is in eraser or brush.)
    <mouseUp>
      Triggered when the mouse being released.
      Do different things in different mode.
      (draw the shape or stoke the line, call cpush at last.)
