window.onload = function()
{
  var c = document.getElementById('canvas');
  c.ctx = c.getContext("2d");
  clearClick();
  var eraser = document.getElementById('EraserButton');
  var brush = document.getElementById('BrushButton');
  var clear = document.getElementById('ClearButton');
  var BruSize = document.getElementById('BrushSize');
  var BruColor = document.getElementById('BrushColor');
  var TextInput = document.getElementById('Text');
  var TextStyle = document.getElementById('font-style');
  var TextSize = document.getElementById('font-size');
  var TextIn = document.getElementById('TextButton');
  var UnDo = document.getElementById('Undo');
  var ReDo = document.getElementById('Redo');
  var Filled = document.getElementById('Filled');
  var Spaced = document.getElementById('Spaced');
  var Triangle = document.getElementById('TriangleButton');
  var Circle = document.getElementById('CircleButton');
  var Rectangle = document.getElementById('RectButton');
  var Download = document.getElementById('download');
  var imgupload= document.getElementById('upload');
  brush.addEventListener("click",brushClick);
  eraser.addEventListener("click",eraserClick);
  clear.addEventListener("click",clearClick);
  c.addEventListener("mousedown",mouseDown);
  c.addEventListener("mousemove",mouseMove);
  c.addEventListener("mouseup",mouseUp);
  window.addEventListener("mouseup",mouseUpOut);
  BruSize.addEventListener("change",bruSizeChange);
  //BruColor.addEventListener("change",bruColorChange);
  TextInput.addEventListener("change",keyInText);
  TextStyle.addEventListener("change",textStyleChange);
  TextSize.addEventListener("change",textSizeChange);
  UnDo.addEventListener("click",unDoClick);
  ReDo.addEventListener("click",reDoClick);
  TextIn.addEventListener("click",textInClick);
  Filled.addEventListener("click",filledChange);
  Spaced.addEventListener("click",filledChange);
  Triangle.addEventListener("click",drawTriangle);
  Circle.addEventListener("click",drawCircle);
  Rectangle.addEventListener("click",drawRect);
  Download.addEventListener("click",downloadS);
  imgupload.addEventListener('change',function()
  {
        var loader = new FileReader();
        loader.onload = function(event)
        {
          var img = new Image();
          img.onload = function()
          {
              document.getElementById('canvas').ctx.drawImage(img,0,0);
          }
          img.src = event.target.result;
        }
        loader.readAsDataURL(this.files[0]);
  });
  c.mode = 0;
  c.ctx.lineWidth = 1;
  c.style.cursor = "url(brush1.png) ,auto";
  c.ctx.strokeStyle = "black";
  c.ctx.fillStyle = "black"
  c.PushArray = new Array();
  c.Step = -1;
  cPush();
  c.Text = "Key words here!";
  c.Filled = false;
  c.xx = 0;
  c.yy = 0;
}
function mouseDown(e)
{
  var o=this;
  this.offsetX=this.offsetLeft;
  this.offsetY=this.offsetTop;
  while(o.offsetParent){
  	o=o.offsetParent;
  	this.offsetX+=o.offsetLeft;
  	this.offsetY+=o.offsetTop;
  }
  this.ctx.beginPath();
  if(this.mode == 1) // Eraser
  {
    this.draw=true;
    this.ctx.moveTo(e.pageX-this.offsetX+7,e.pageY-this.offsetY+37);
    this.ctx.lineWidth = 10;
  }
  else if(this.mode == 2) //Text
  {
    this.ctx.strokeStyle = document.getElementById('BrushColor').value;
    this.ctx.fillStyle = document.getElementById('BrushColor').value;
    this.ctx.lineWidth = 1;
    if(this.Filled)
      this.ctx.fillText(document.getElementById('Text').value,e.pageX-this.offsetX,e.pageY-this.offsetY);
    else
      this.ctx.strokeText(document.getElementById('Text').value,e.pageX-this.offsetX,e.pageY-this.offsetY);
  }
  else if(this.mode == 3) //Triangle
  {
    this.ctx.moveTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
    this.xx = e.pageX-this.offsetX;
    this.yy = e.pageY-this.offsetY;
    this.ctx.strokeStyle = document.getElementById('BrushColor').value;
    this.ctx.fillStyle = document.getElementById('BrushColor').value;
  }
  else if(this.mode == 4) //circle
  {
    this.xx = e.pageX-this.offsetX;
    this.yy = e.pageY-this.offsetY;
    this.ctx.strokeStyle = document.getElementById('BrushColor').value;
    this.ctx.fillStyle = document.getElementById('BrushColor').value;
  }
  else if(this.mode == 5) //rectangle
  {
    this.xx = e.pageX-this.offsetX;
    this.yy = e.pageY-this.offsetY;
    this.ctx.strokeStyle = document.getElementById('BrushColor').value;
    this.ctx.fillStyle = document.getElementById('BrushColor').value;
  }
  else //drawing
  {
    this.draw=true;
    this.ctx.moveTo(e.pageX-this.offsetX,e.pageY-this.offsetY+45);
    this.ctx.lineWidth = document.getElementById('BrushSize').value;
    this.ctx.strokeStyle = document.getElementById('BrushColor').value;
    this.ctx.fillStyle = document.getElementById('BrushColor').value;
  }
}
function mouseMove(e)
{
  if(this.draw)
  {
    if(this.mode == 1)
      this.ctx.lineTo(e.pageX-this.offsetX+7,e.pageY-this.offsetY+37)
    else
      this.ctx.lineTo(e.pageX-this.offsetX,e.pageY-this.offsetY+45);
    this.ctx.stroke();
  }
}
function mouseUp(e)
{
  var c = document.getElementById('canvas');
  if(this.draw)
  {
    this.ctx.closePath();
    this.draw=false;
  }
  else if(this.mode == 3)
  {
    this.ctx.lineTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
    this.ctx.lineTo(2*this.xx-e.pageX+this.offsetX,e.pageY-this.offsetY);
    if(this.Filled)
      this.ctx.fill();
    else
    {
      this.ctx.lineTo(this.xx,this.yy);
      this.ctx.stroke();
      this.ctx.closePath();
    }
  }
  else if(this.mode == 4)
  {
    //this.ctx.beginPath();
    this.ctx.arc((this.xx+e.pageX-this.offsetX)/2,(this.yy+e.pageY-this.offsetY)/2,distance(this.xx,this.yy,e.pageX-this.offsetX,e.pageY-this.offsetY),0,2*Math.PI);
    if(this.Filled)
      this.ctx.fill();
    else
    {
        this.ctx.stroke();
        this.ctx.closePath();
    }
  }
  else if(this.mode == 5)
  {
    if(this.xx-e.pageX+this.offsetX > 0 && this.yy - e.pageY+this.offsetY > 0)
      this.ctx.rect(e.pageX-this.offsetX,e.pageY-this.offsetY,Math.abs(this.xx-e.pageX+this.offsetX),Math.abs(this.yy-e.pageY+this.offsetY));
    else if(this.xx-e.pageX+this.offsetX < 0 && this.yy-e.pageY+this.offsetY < 0)
      this.ctx.rect(this.xx,this.yy,Math.abs(this.xx-e.pageX+this.offsetX),Math.abs(this.yy-e.pageY+this.offsetY));
    else if(this.xx-e.pageX+this.offsetX > 0 && this.yy-e.pageY+this.offsetY < 0)
      this.ctx.rect(e.pageX-this.offsetX,this.yy,Math.abs(this.xx-e.pageX+this.offsetX),Math.abs(this.yy-e.pageY+this.offsetY));
    else
      this.ctx.rect(this.xx,e.pageY-this.offsetY,Math.abs(this.xx-e.pageX+this.offsetX),Math.abs(this.yy-e.pageY+this.offsetY));
    if(this.Filled)
      this.ctx.fill();
    else
    {
        this.ctx.stroke();
        this.ctx.closePath();
    }
  }
  cPush();
}
function eraserClick()
{
  var c = document.getElementById('canvas');
  c.mode = 1;
  c.ctx.strokeStyle = "#ffffff";
  c.ctx.lineWidth = 10;
  c.style.cursor = "url(eraser1.png) ,auto";
}
function brushClick()
{
  var c = document.getElementById('canvas');
  c.mode = 0;
  c.style.cursor = "url(brush1.png) ,auto";
  c.ctx.strokeStyle = document.getElementById('BrushColor').value;
  c.ctx.lineWidth = document.getElementById('BrushSize').value;
}
function clearClick()
{
  var c = document.getElementById('canvas');
  c.ctx.closePath();
  c.ctx.clearRect(0,0,c.width,c.height);
  //c.ctx.strokeStyle = document.getElementById('BrushColor').value;
  //c.ctx.lineWidth = document.getElementById('BrushSize').value;
  c.style.cursor = "url(brush1.png) ,auto";
  c.mode = 0;
  c.ctx.fillStyle = "white";
  c.ctx.fillRect(0,0,c.width,c.height);
  c.ctx.strokeStyle = document.getElementById('BrushColor').value;
  c.ctx.fillStyle = document.getElementById('BrushColor').value;
}
function mouseUpOut()
{
  var c = document.getElementById('canvas');
  if(c.draw)
  {
    c.ctx.closePath();
    cPush();
    c.draw = false;
  }
}
function bruSizeChange()
{
  var c = document.getElementById('canvas');
  if(c.mode != 1)
    c.ctx.lineWidth = this.value;
}
/*function bruColorChange()
{
  if(mode != 1)
  {
    var c = document.getElementById('canvas');
    c.ctx.strokeStyle = this.value;
    c.ctx.fillStyle = this.value;
  }
}*/
function keyInText()
{
  c.Text = this.value;
}
function textStyleChange()
{
  var c = document.getElementById('canvas');
  var x = "" + String(document.getElementById('font-size').value)+"px ";
  x += String(this.value);
  c.ctx.font = x;
}
function textSizeChange()
{
  var c = document.getElementById('canvas');
  var x = "" + String(this.value) +"px ";
  x += String(document.getElementById('font-style').value);
  c.ctx.font = x;
}
function unDoClick()
{
    var c = document.getElementById('canvas');
  if (c.Step > 0) {
      c.Step--;
      var canvasPic = new Image();
      canvasPic.src = c.PushArray[c.Step];
      clearClick();
      canvasPic.onload = function () { c.ctx.drawImage(canvasPic, 0, 0); }
  }
    //console.log(c.Step);
    //console.log(canvasPic.src);
}
function reDoClick()
{
  var c = document.getElementById('canvas');
  if (c.Step < c.PushArray.length-1)
  {
    c.Step++;
    var CanvasPic = new Image();
    CanvasPic.src = c.PushArray[c.Step];
    CanvasPic.onload = function () { c.ctx.drawImage(CanvasPic, 0, 0); };
    console.log("Redo",c.Step);
  }
}
function textInClick()
{
  var c = document.getElementById('canvas');
  c.style.cursor = "crosshair";
  c.mode = 2;
}
function filledChange()
{
  var c = document.getElementById('canvas');
  if(this === document.getElementById('Filled'))
    c.Filled = true;
  else
    c.Filled = false;
}
function drawTriangle()
{
  var c = document.getElementById('canvas');
  c.mode = 3;
  c.style.cursor = "crosshair";
}
function drawCircle()
{
  var c = document.getElementById('canvas');
  c.mode = 4;
  c.style.cursor = "crosshair";
}
function drawRect()
{
  var c = document.getElementById('canvas');
  c.mode = 5;
  c.style.cursor = "crosshair";
}
function distance(a,b,c,d)
{
  var s = Math.pow(Math.abs(a-c),2);
  var t = Math.pow(Math.abs(b-d),2);
  s += t;
  t = Math.sqrt(s);
  return t/2;
}
function cPush()
{
  var c = document.getElementById('canvas');
  c.Step++;
  c.PushArray[c.Step] = c.toDataURL();
}
function downloadCanvas(link,filename)
{
  link.href = document.getElementById('canvas').toDataURL();
  link.download = filename;
}
function downloadS()
{
  downloadCanvas(this,Math.random().toString(36).substr(3,9));
}
